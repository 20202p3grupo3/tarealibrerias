#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "utilfecha.h"


int main(int argc, char **argv){
 int option;
 int sflag = 0;
 int dflag = 0;
 int fflag = 0;
 while ((option = getopt(argc, argv, "s:d:f:"))!= -1){
    switch(option){
       case 's':
	       if(sflag){
                   printf("Solo puede escoger una opcion");
                   exit(1);
	       } else{
		       sflag++;
		       dflag++;
		       fflag++;
		      }
	       secToHMS(atoi(optarg));
	       break;
        case 'd':
	       if(dflag){
	          printf("Solo puede escoger una opcion");
	          exit(1);
	       } else{
		       sflag++;
		       dflag++;
		       fflag++;
	             }
	       dayToYMD(atoi(optarg));
	       break;
        case 'f':
	       if(fflag){
		       printf("Solo puede escoger una opcion");
		       exit(1);
	       } else{


		     sflag++;
		     dflag++;
		     fflag++;
	       }
	       formatDate(optarg);
	       break;
	default:
	       printf("Error, no escogió ninguna opción");
    
    
    }
 
 
 }

}


