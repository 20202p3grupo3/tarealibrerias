#include <stdio.h> 
#define _XOPEN_SOURCE 700
#include <time.h>

void formatDate(char date[20]){
	int i;
	struct tm tm={0};
	char *s=strptime(date,"%Y-%m-%d %H:%M:%S",&tm);
	const char * months[12]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	printf("%d de %s de %d , %d:%d:%d \n",tm.tm_mday,months[tm.tm_mon],tm.tm_year+1900,tm.tm_hour,tm.tm_min,tm.tm_sec);
}



