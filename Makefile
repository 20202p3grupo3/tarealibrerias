all: bin/statexe bin/dynamexe

bin/statexe: obj/main.o lib/libestatica.a
	gcc -static obj/main.o -L lib/ -lestatica -o bin/statexe

bin/dynamexe: obj/main.o lib/libdinamica.so
	gcc obj/main.o -L lib/ -ldinamica -o bin/dynamexe

lib/libdinamica.so: src/segundos.c src/formato.c src/dias.c
	gcc -fPIC -shared src/segundos.c src/formato.c src/dias.c -o lib/libdinamica.so

lib/libestatica.a: obj/segundos.o obj/formato.o obj/dias.o
	ar rcs lib/libestatica.a obj/segundos.o obj/formato.o obj/dias.o

obj/main.o: src/main.c
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

obj/segundos.o: src/segundos.c
	gcc -Wall -c src/segundos.c -o obj/segundos.o

obj/formato.o: src/formato.c
	gcc -Wall -c src/formato.c -o obj/formato.o

obj/dias.o: src/dias.c
	gcc -Wall -c src/dias.c -o obj/dias.o



.PHONY: clean
clean:
	rm obj/* bin/* lib/*



