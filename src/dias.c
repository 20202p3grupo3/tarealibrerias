#include <stdio.h>

void dayToYMD(int days){
    int y,m,d;
    y = (int) days/360; 
    days = days - (360*y);
    m = (int) days/30;
    d = (int) days - (m*30);
    printf("años   meses dias\n%d        %d      %d \n",y,m,d);
}
